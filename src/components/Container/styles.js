import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  padding: 10px;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`
