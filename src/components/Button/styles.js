import styled, { css } from 'styled-components'

const textColor = ({ theme }) => theme.text

const outlined = ({ theme, variant, outline }) => {
  const color = theme.colors[variant]
  return outline
    ? css`
        color: ${color};
        border: 2px solid ${color};
        background-color: transparent;
        :hover {
          color: #fff;
          background-color: ${color};
          box-shadow: 1px 1px 10px ${color};
        }
      `
    : ''
}

const buttonColorVariant = ({ theme, variant }) => {
  const color = theme.colors[variant] ?? theme.colors['primary']
  return css`
    color: #fff;
    background-color: ${color};
    border: 2px solid ${color};
    :hover {
      box-shadow: 1px 1px 10px ${color};
    }
  `
}

export const Button = styled.button`
  margin: 5px;
  padding: 5px;
  font-size: 16px;
  cursor: pointer;
  font-weight: 600;
  min-width: 100px;
  text-align: center;
  border-radius: 4px;
  color: ${textColor};
  text-transform: uppercase;
${'' /* a sequencia abaixo é obrigatória para nao haver sobrescrição de estilos */}
  ${buttonColorVariant}
  ${outlined}
`

// <=================== outra forma commun de se implementar ==================>
const primaryColor = ({ theme }) => theme.colors.primary
const secondaryColor = ({ theme }) => theme.colors.secondary
const tertiaryColor = ({ theme }) => theme.colors.tertiary
const skyieColor = ({ theme }) => theme.colors.skyie

const primary = css`
  color: #fff;
  border: 2px solid ${primaryColor};
  background-color: ${primaryColor};
  :hover {
    color: #fff;
    box-shadow: 1px 1px 10px ${primaryColor};
  }
`
const secondary = css`
  color: #fff;
  border: 2px solid ${secondaryColor};
  background-color: ${secondaryColor};
  :hover {
    color: #fff;
    box-shadow: 1px 1px 10px ${secondaryColor};
  }
`
const tertiary = css`
  color: #fff;
  border: 2px solid ${tertiaryColor};
  background-color: ${tertiaryColor};
  :hover {
    color: #fff;
    box-shadow: 1px 1px 10px ${tertiaryColor};
  }
`
const skyie = css`
  color: #fff;
  border-radius: 4px;
  border: 2px solid ${skyieColor};
  background-color: ${skyieColor};
  :hover {
    color: ${skyieColor};
    box-shadow: 1px 1px 10px ${skyieColor};
  }
`
const dark = css``

const variants = {
  primary,
  secondary,
  tertiary,
  skyie,
  dark
}

export const SButton = styled.button`
  margin: 5px;
  padding: 5px;
  font-size: 16px;
  cursor: pointer;
  font-weight: 600;
  text-align: center;
  border-radius: 4px;
  color: ${textColor};
  text-transform: uppercase;
  ${({ variant }) => (variant ? variants[variant] : primary)}
`
