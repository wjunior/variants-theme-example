import React from 'react'
import * as S from './styles'

const Button = ({ onClick, label, variant, outline }) => {
  return (
    <S.Button onClick={onClick} variant={variant} outline={outline}>
      {label}
    </S.Button>
  )
}
export default Button
