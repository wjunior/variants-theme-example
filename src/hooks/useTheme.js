import { useState } from 'react'
import { dark, light } from '../styles'

const useTheme = () => {
  const [theme, setTheme] = useState(light)
  const [current, changeTheme] = useState('dark')
  const themes = {
    dark,
    light
  }
  const toggleTheme = () => {
    const nextTheme = current === 'light' ? 'dark' : 'light'
    changeTheme(nextTheme)
  }
  return [themes[current], toggleTheme]
}

export default useTheme
