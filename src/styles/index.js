import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
  }
  html, body {
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${({ theme }) => theme.background};
  }
`

export const dark = {
  text: '#e8e6e3',
  background: '#181a1b',
  colors: {
    primary: '#038857',
    secondary: '#ffd548',
    tertiary: '#9c2a00',
    skyie: '#22bad9',
    white: '#e8e6e3'
  }
}
export const light = {
  text: '#181a1b',
  background: '#e8e6e3',
  colors: {
    primary: '#038857',
    secondary: '#ffd548',
    tertiary: '#9c2a00',
    skyie: '#22bad9'
  }
}

export default GlobalStyle
