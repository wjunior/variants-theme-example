import React from 'react'
import * as ReactDOM from 'react-dom/client'
import { ThemeProvider } from 'styled-components'

import { useTheme } from './hooks'
import { Container, Button } from './components'
import GlobalStyle from './styles'

const main = document.getElementById('root')
const root = ReactDOM.createRoot(main)

const App = () => {
  const [theme, toggleTheme] = useTheme()
  return (
    <ThemeProvider theme={theme}>
      <Container>
        <Button label="skyie variant" variant="skyie" />
        <Button label="skyie outline" variant="skyie" outline />
        <Button label="primary variant" variant="primary" />
        <Button label="primary outline" variant="primary" outline />
        <Button label="secondary variant" variant="secondary" />
        <Button label="secondary outline" variant="secondary" outline />
        <Button label="tertiary variant" variant="tertiary" />
        <Button label="tertiary outline" variant="tertiary" outline />
        <Button label="default variant" />
        <Button onClick={toggleTheme} label="Trocar tema" variant="secondary" outline />
        <Button onClick={toggleTheme} label="W" variant="secondary" outline />
        <GlobalStyle />
      </Container>
    </ThemeProvider>
  )
}
root.render(<App />)
